**Hot Reload vs Hot Restart**

******Hot Reload :::
    
Hot Reload allows us to see the reflected change after bug fixes, building User interfaces and even adding certain features to the app without running your application afresh over and over again.

When Hot Reload is invoked, the host machine checks the edited code since the last compilation and recompiles that again.

Hot Reload does not work when Enumerated types are changed to regular Classes and when classes is changed to enumerated types

Hot Reload does not work when generic types are modified

****************Hot Restart :::

Hot restart destroys the preserved State value and set them to their default.
Hot Restart takes much higher time than Hot reload.
Hot reload is also know as ‘stateful hot reload’
Hot Reload is useful because it saves time by just implementing the functionality based on the closest build class in less than 10secs.


**************Flutter Custom Widget************
We ou can create custom widgets where we select a chart, set data definition to add one or more data sources, select monitoring parameters, and set widget-level filters. You can create such custom widgets and use them in different dashboards according to your requirement.

Example:::

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
  
class CustomContainer extends StatelessWidget {
  CustomContainer(
      {@required this.child, this.height, this.width, this.onTap, this.color});
  final Function onTap;
  final Widget child;
  final double height;
  final double width;
  final Color color;
  
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: height,
        width: width,
        padding: EdgeInsets.all(12),
        decoration: BoxDecoration(
            color: color, borderRadius: BorderRadius.all(Radius.circular(8))),
        child: child,
      ),
    );
  }
}


************* Platform Specific Code **************

Platform channels provide a simple mechanism for communicating between your Dart code and the platform-specific code of your host app. This means you can expose a platform service in your host app code and have it invoked from the Dart side. Or vice versa.

***********Event loop vs Isolates
 Event Loop : A Dart app has a single event loop with two queues—the event queue and the microtask queue.

The event queue contains all outside events: I/O, mouse events, drawing events, timers, messages between Dart isolates, and so on.

The microtask queue is necessary because event-handling code sometimes needs to complete a task later, but before returning control to the event loop. For example, when an observable object changes, it groups several mutation changes together and reports them asychronously. The microtask queue allows the observable object to report these mutation changes before the DOM can show the inconsistent state.

The event queue contains events both from Dart and from elsewhere in the system. Currently, the microtask queue contains only entries originating from within Dart code, but we expect the web implementation to plug into the browser microtask queue.

Isolates : An isolate is an abstraction on top of threads. It is similar to an event loop, with a few differences:

An isolate has its own memory space
It cannot share mutable values with other isolates
Any data transmitted between isolates is duplicated
An isolate is meant to run independently of other isolates. This offers a lot of benefits to the Dart VM, one of which is that garbage collection is easier.

One thing to keep in mind about creating parent isolates that, in turn, create child isolates is that the child isolates will terminate if the parent does. Regardless of the hierarchy, the parent isolate cannot access the memory of the child isolate.

















